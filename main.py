from tg import TGController, AppConfig, expose, flash, require, url, request, redirect
from tg import MinimalApplicationConfigurator
from wsgiref.simple_server import make_server

from tg.configurator.components.statics import StaticsConfigurationComponent

import time

class RootController(TGController):
    
    @expose()
    def index(self):
        return "Hello, World!"

    @expose("index.xhtml")
    def page(self):
        return dict(data= None)

config = AppConfig(minimal=True, root_controller=RootController())
config.renderers = ['kajiki']
config.serve_static = True
config.paths['static_files'] = 'static'

application = config.make_wsgi_app()



if __name__ == "__main__":
	print("Welcome to PROJECT_UNNAMED")
	print("Serving on port 8080...")
	httpd = make_server('', 8080, application)
	httpd.serve_forever()
else:
	print("This file should not be imported into another project, as it is the executable for another. \nPlease run this file as intended.")
	exit(1)